# Shared files for multiple templates

These files are shared across multiple latex templates and contain general shared information such as name or university.

If you came here to clone one of the latex templates found (here)[] it is advised to fork this repository.

After you forked the repository edit `person.tex` and fill in you personal information.

Use your repository fork as submodule for the templates and your shared data will be available for each template.